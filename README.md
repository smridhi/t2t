# MazeCraft

Team :
She-gineers::  Smridhi Sharma , Devika Malik

Mazecraft is a Haskell project that implements pathfinding algorithms for navigating mazes. It utilizes both Prim's algorithm for generating a minimum spanning tree (MST) to represent the maze structure and the Prim's search algorithm for finding the optimal path from a start point to a goal within the maze.

# Features:
- User can play with a MAZE size ranging from 2 to 50.
- This project includes seed functionality which enables the generation of a different Maze the moment user completes the current Maze.

# Maze Generation:

Generates a maze using Prim's algorithm, ensuring an efficient representation of the maze structure.

- What is Prim's Algorithm ?

Prim's algorithm generates mazes by treating the maze as a graph. It begins with a grid of cells, all initially walls. A random cell is selected as the starting point, marked as part of the maze, and its walls are added to a priority queue. The algorithm removes the wall with the lowest weight (or randomly) from the queue. If the cell on the opposite side isn't part of the maze, the wall becomes a passage, and the cell is added to the maze. Its neighboring walls are then added to the queue. This continues until all cells are connected, creating a fully connected, loop-free maze.

- Why Prim's Algorithm?

  - **Randomization:** Prim's algorithm can be adapted to choose edges randomly, which helps in creating an unpredictable and
    complex maze structure.
  - **Connectivity:** It guarantees that the maze will be fully connected, meaning there will be a path between any points in the maze.
  - **Efficiency in Handling Walls and Cells:** The code uses HashSet to efficiently manage the cells and walls, allowing for
    quick lookups and insertions. It maintains a list of walls and processes each wall only once, ensuring no redundant operations.



# Haskell Implementation:

Leverages the power and expressiveness of Haskell for a clean and functional approach to maze navigation.
- Why Haskell ?
  - **Lazy Evaluation:** Haskell's lazy evaluation model allows for the efficient handling of large data structures and computations, such as generating and  processing a large grid of cells and walls.
  - **Efficient Handling of Sets and Maps:** Haskell's libraries, such as Data.HashSet and Data.HashMap, provide efficient implementations for sets and maps, which are crucial for managing the cells and walls in the maze generation algorithm.


# How to run this project:

This project works on the command line prompt with ghci :

 - ghci MazeCli.hs
 - in ghci -> main
