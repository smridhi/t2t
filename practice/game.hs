import Data.Char
import System.IO


data GameState = GameState {
  word :: String,
  guessed :: String,
  attempts :: Int
} deriving Show

initGameState :: String -> GameState
initGameState word = GameState word (replicate (length word) '_') 10


drawHangman :: Int -> String
drawHangman 0 = " _______\n|       |\n|       O\n|      /|\\\n|      / \\\n|"
drawHangman 1 = " _______\n|       |\n|       O\n|      /|\\\n|      /\n|"
drawHangman 2 = " _______\n|       |\n|       O\n|      /|\\\n|       \n|"
drawHangman 3 = " _______\n|       |\n|       O\n|      /|\n|       \n|"
drawHangman 4 = " _______\n|       |\n|       O\n|       |\n|       \n|"
drawHangman 5 = " _______\n|       |\n|       O\n|       \n|       \n|"
drawHangman 6 = " _______\n|       |\n|       \n|       \n|       \n|"
drawHangman _ = " _______\n|       |\n|       \n|       \n|       \n|"


updateGameState :: GameState -> Char -> GameState
updateGameState gs c =
  let word' = word gs
      guessed' = guessed gs
      attempts' = attempts gs
      newGuessed = zipWith (\x y -> if x == c then x else y) word' guessed'
  in if c `elem` word'
     then gs { guessed = newGuessed }
     else gs { attempts = attempts' - 1, guessed = newGuessed }


checkGameState :: GameState -> IO ()
checkGameState gs =
  let word' = word gs
      guessed' = guessed gs
      attempts' = attempts gs
  in if all (== word') guessed'
     then putStrLn "Congratulations, you won!"
     else if attempts' == 0
          then putStrLn "Game over, the word was " ++ word'
          else return ()


playGame :: GameState -> IO ()
playGame gs = do
  putStrLn $ "Word: " ++ guessed gs
  putStrLn $ "Attempts left: " ++ show (attempts gs)
  putStrLn $ drawHangman (10 - attempts gs)
  putStr "Guess a letter: "
  hFlush stdout
  c <- getChar
  putStrLn ""
  let gs' = updateGameState gs (toLower c)
  checkGameState gs'
  if attempts gs' > 0 && all (/= '_') (guessed gs')
    then return ()
    else playGame gs'


main :: IO ()
main = do
  putStrLn "Welcome to Hangman!"
  putStrLn "I'm thinking of a word..."
  let gs = initGameState "haskell"
  playGame gs
