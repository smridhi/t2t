import Data.List
import Data.Ord

-- Define a data type for a graph
data Graph = Graph [(Int, Int, Int)] deriving Show

-- Define a function to create a graph from a list of edges
createGraph :: [(Int, Int, Int)] -> Graph
createGraph edges = Graph edges

-- Define a function to find the minimum spanning tree using Prim's algorithm
prim :: Graph -> [(Int, Int, Int)]
prim (Graph edges) = prim' [] edges
  where
    prim' :: [(Int, Int, Int)] -> [(Int, Int, Int)] -> [(Int, Int, Int)]
    prim' mst [] = mst
    prim' mst edges =
      let (u, v, w) = minimumBy (comparing (\(_, _, w) -> w)) edges
          edges' = filter (\(x, y, _) -> x /= u && y /= u) edges
          mst' = (u, v, w) : mst
      in prim' mst' edges'

-- Define a function to print the minimum spanning tree
printMST :: [(Int, Int, Int)] -> IO ()
printMST mst = mapM_ (\(u, v, w) -> putStrLn $ "Edge: " ++ show u ++ " - " ++ show v ++ ", Weight: " ++ show w) mst

-- Create a sample graph
graph :: Graph
graph = createGraph [(1, 2, 2), (1, 3, 3), (2, 3, 1), (2, 4, 1), (3, 4, 4)]

-- Run Prim's algorithm and print the minimum spanning tree
main :: IO ()
main = printMST $ prim graph
