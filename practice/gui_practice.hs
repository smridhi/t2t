import qualified GI.Gtk as Gtk

main :: IO ()
main = do
  -- Initialize GTK
  Gtk.init Nothing

  -- Create a window
  window <- Gtk.windowNew [Gtk.windowType := Gtk.WindowTypeToplevel]

  
  Gtk.set window [Gtk.windowTitle := "My GUI"]

  -- Create a button
  button <- Gtk.buttonNewWithLabel "Click me!"

  -- Add the button to the window
  Gtk.containerAdd window button

  -- Show the window and all its children
  Gtk.widgetShowAll window

  -- Run the main loop
  Gtk.main
