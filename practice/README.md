# Haskell Practice Repository
================================

This repository contains a collection of basic Haskell programs that I have written to practice and learn the fundamentals of the language.

## Contents
------------

### Basic Programs

* **Fibonacci**: A program to calculate the nth Fibonacci number using recursion and memoization.
* **Factorial**: A program to calculate the factorial of a given number using recursion and pattern matching.
* **Sum of a List**: A program to calculate the sum of a list of numbers using recursion and higher-order functions.
* **Reverse a List**: A program to reverse a list of elements using recursion and pattern matching.
* **Check if a Number is Prime**: A program to check if a given number is prime using recursion and pattern matching.

### GUI Programming with GTK

* **GTK Hello World**: A basic GUI program that displays a window with a label using the GTK library.
* **GTK Button Click**: A program that demonstrates how to handle button clicks and update the GUI using GTK.

### Graph Algorithms

* **Prim's Algorithm**: An implementation of Prim's algorithm for finding the minimum spanning tree of a graph using Haskell.

## Learning Outcomes
-------------------

Through this practice, I have gained a solid understanding of the following Haskell concepts:

* Functional programming principles
* Recursion and memoization
* Pattern matching and guards
* Higher-order functions and lambda expressions
* Basic GUI programming using GTK
* Graph algorithms and data structures

