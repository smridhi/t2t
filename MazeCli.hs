module MazeCli where

import Data.Char
import System.Environment
import System.Exit
import System.Random
import Maze
import MazeUtils
import GUI

main :: IO()
main = do
   args <- getArgs
   verifyArgsOrQuit args
   seed <- getSeed args
   putStrLn "---"
   putStrLn "Welcome to MazeCraft!"
   putStrLn "---"
   putStrLn "Controls:"
   putStrLn "Use WASD or Arrow Keys to make your way to the end of the maze."
   putStrLn "Press R to reset your position to the start of the maze."
   putStrLn "Press Escape to close the game."
   putStrLn "---"
   putStrLn "Game Settings:"
   showSeed seed
   putStrLn "Please enter the grid size (2 <= x <= 50):"
   mazeSize <- getNum "Please enter a valid integer (2 <= x <= 50):"
   let (maze, gridInteger, nextGen) = createMaze mazeSize (getRandomGen seed)
   createGUI maze gridInteger nextGen

getNum :: String -> IO Int
getNum prompt =
  getFromStdin prompt getLine validateGridSize read

getFromStdin :: String -> (IO a) -> (a -> Bool) -> (a -> b) -> IO b
getFromStdin prompt inputF isOk transformOk = do
  input <- inputF
  if isOk input
     then return $ transformOk input
     else do
       putStrLn prompt
       getFromStdin prompt inputF isOk transformOk

getRandomGen :: Int -> StdGen
getRandomGen seed = mkStdGen seed

getSeed :: [String] -> IO Int
getSeed [] = getRandomSeed
getSeed (x:_) = return $ read x

getRandomSeed :: IO Int
getRandomSeed = do
  randomSrc <- getStdGen
  return $ fst $ random $ randomSrc


showSeed :: Int -> IO ()
showSeed seed = putStrLn $ "The random seed is " ++ show seed ++ "."


verifyArgsOrQuit :: [String] -> IO ()
verifyArgsOrQuit args =
  if verifyArgs args
     then putStrLn "args ok!"
     else exitWithBadArgs

exitWithBadArgs :: IO ()
exitWithBadArgs = do
  progName <- getProgName
  putStrLn $ "Arg must be an Integer."
  putStrLn $ "Use: " ++  progName ++ " [optional seed]"
  exitWith $ ExitFailure 1

verifyArgs :: [String] -> Bool
verifyArgs [] = True
verifyArgs [['-']] = False
verifyArgs (x:xs) = Prelude.null xs && isInt x

validateGridSize :: String -> Bool
validateGridSize [] = False
validateGridSize ['-'] = False
validateGridSize input = isInt input && (read input <= 50) && (read input >= 2)

isInt :: String -> Bool
isInt [] = False
isInt (x:xs) = all isDigit xs && (x == '-' || isDigit x)
